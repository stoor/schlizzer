import serial
import time
import argparse
import re

from sys import exit

####################################################################
#
# Usage: converter.py <arduino port> <gcode file> [--verbose]
#
# For eg: python ./converter.py /dev/ttys1 /path/to/cube1.gcode
#
####################################################################


parser = argparse.ArgumentParser()
parser.add_argument("port", help="port at which arduino is connected")
parser.add_argument("gfile", help="path to a gcode file")
parser.add_argument("-v", "--verbose", action="store_true", help="increase output verbosity")
parser.add_argument("-t", "--test", action="store_true", help="test gcode parsing without connecting to arduino")
args = parser.parse_args()

arduinoPort = args.port
gcodeFile = args.gfile


BAUD_RATE = 9600

line_num = 0

#be careful while changing these globals. Only change them inside parseGcode() method
PX = -999
PY = -999
PZ = -999
E = -999
F = -999

re_m = re.compile(r'^M.*$')
re_g = re.compile(r'^G.*$')
re_g1 = re.compile(r'^G1.*$')
re_x = re.compile(r'^X(.*)$')
re_y = re.compile(r'^Y(.*)$')
re_z = re.compile(r'^Z(.*)$')
re_e = re.compile(r'^E(.*)$')
re_f = re.compile(r'^F(.*)$')

def parseGcode(gcode):
  global line_num
  global PX
  global PY
  global PZ
  global E
  global F
  line_num += 1

  match_g = re_g.search(gcode)
  if match_g:
    match_g1 = re_g1.search(match_g.group())
    if match_g1:
      codes = re.split(' ', match_g1.group())
      codes = codes[1:len(codes)]
      for c in codes:
        match_pt = re_x.search(c)
        if match_pt:
          PX = match_pt.group(1)
          continue

        match_pt = re_y.search(c)
        if match_pt:
          PY = match_pt.group(1)
          continue

        match_pt = re_z.search(c)
        if match_pt:
          PZ = match_pt.group(1)
          continue

        match_pt = re_e.search(c)
        if match_pt:
          E = match_pt.group(1)
          continue

        match_pt = re_f.search(c)
        if match_pt:
          F = match_pt.group(1)
    else:
      pass
  else:
    pass

arduino = None
if not args.test:
  try:
    print "Connecting to {}".format(arduinoPort)
    arduino = serial.Serial(arduinoPort, BAUD_RATE)
  except Exception, e:
    print "Failed to connect to {}".format(arduinoPort)
    print "Error: {}".format(e)
    exit()


try:
  with open(gcodeFile, 'r') as f:
    for line in f:
      parseGcode(line)
      data = str(PX)+','+str(PY)+','+str(PZ)+','+str(E)+','+str(F);
      if args.verbose:
        print "Sending data=",data
      if not args.test:
        arduino.write(data)
        time.sleep(1)
        if args.verbose: 
          print arduino.readline()
except Exception, e:
  print "Failed to send X={} Y={} Z={} E={} F={} to arduino".format(PX, PY, PZ, E, F)
  print "Error: {}".format(e)


